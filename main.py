import json
import os
import xml.etree.ElementTree as xml
from json2xml import json2xml

def Convertjson2xml(loc):

    in_json = open (loc, "r")
    jsondata = json.loads(in_json.read())
    js = json2xml.Json2xml(jsondata, pretty=True, attr_type=False).to_xml()
    catalog = list(xml.fromstring(js))[0]
    p = xml.tostring(catalog, encoding='utf-8', method='xml').decode('utf8')
    f = open(os.path.splitext(loc)[0] + ".xml", "w")
    f.write('<?xml version="1.0" ?>\n\t' + p)
    f.close()

loc = r"MyFile.json"
Convertjson2xml(loc)